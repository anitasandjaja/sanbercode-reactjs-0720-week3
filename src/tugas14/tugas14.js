import React, {useState, useEffect} from "react"
import axios from "axios"
import "./tugas14.css"

const Lists = () =>{
    const [dataHargaBuah, setDataHargaBuah] = useState(null)
    const [inputNama, setInputNama] = useState("")
    const [inputHarga, setInputHarga] = useState("")
    const [inputBerat, setInputBerat] = useState("")
    const [selectedId, setSelectedId]  =  useState(0)
    const [statusForm, setStatusForm]  =  useState("create")

    useEffect( () => {
        if (dataHargaBuah === null){
          axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
          .then(res => {
            setDataHargaBuah(res.data.map(el=>{ return {id: el.id, nama: el.name, harga: el.price, berat: el.weight}} ))
          })
        }
      }, [dataHargaBuah])
      
      const handleDelete = (event) => {
        let idBuah = parseInt(event.target.value)
    
        let newDataHargaBuah = dataHargaBuah.filter(el => el.id !== idBuah)
    
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
        .then(res => {
          console.log(res)
        })
              
        setDataHargaBuah([...newDataHargaBuah])
        
      }

      const handleEdit = (event) =>{
        let idBuah = parseInt(event.target.value)
        let buah = dataHargaBuah.find(x=> x.id === idBuah)
        setInputNama(buah.nama)
        setInputHarga(buah.harga)
        setInputBerat(buah.berat)
        setSelectedId(idBuah)
        setStatusForm("edit")
      }
    
      const handleChange = (event) =>{
          let typeOfInput = event.target.name
          switch (typeOfInput){
                case 'nama':
                  setInputNama(event.target.value)
                  break
                case 'harga':
                  setInputHarga(event.target.value)
                  break
                case 'berat':
                  setInputBerat(event.target.value)
                  break
                default: break
          }
      }
    
      const handleSubmit = (event) =>{
        // menahan submit
        event.preventDefault()
    
        let name = inputNama
        let price = inputHarga
        let weight = inputBerat
    
        if (name.replace(/\s/g,'') !== "" && price.toString().replace(/\s/g,'') !== "" && weight.toString().replace(/\s/g,'') !== ""){      
          if (statusForm === "create"){        
            axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name, price, weight})
            .then(res => {
                setDataHargaBuah([...dataHargaBuah, {id: res.data.id, nama: name, harga: price, berat: weight}])
            })
          }else if(statusForm === "edit"){
            axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, {name, price, weight})
            .then(res => {
                let dataBuah = dataHargaBuah.find(el=> el.id === selectedId)
                dataBuah.nama = name
                dataBuah.harga = price
                dataBuah.berat = weight
                setDataHargaBuah([...dataHargaBuah])
            })
          }
          
          setStatusForm("create")
          setSelectedId(0)
          setInputNama("")
          setInputHarga("")
          setInputBerat("")
        }
    }    
    
    return(
        <>
        <div className='table'>
        <h1>Tabel Harga Buah</h1>
          <table>
            <thead>
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                {
                  dataHargaBuah !== null && dataHargaBuah.map((val, index)=>{
                    return(                    
                      <tr key={index}>
                        <td>{val.nama}</td>
                        <td>{val.harga}</td>
                        <td>{val.berat/1000} Kg</td>
                        <td>
                          <button onClick={handleEdit} value={val.id}>Edit</button>
                          &nbsp;
                          <button onClick={handleDelete} value={val.id}>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>
        </div>
          {/* Form */}
          <h1>Form Tambah Data Buah</h1>
          <div className='form'>
            <form onSubmit={handleSubmit}>
              <label>
                Masukkan Nama Buah:
              </label>          
              <input type="text" name="nama" value={inputNama} onChange={handleChange}/>&nbsp;
              <label>
                Masukkan Harga Buah:
              </label>          
              <input type="text" name="harga" value={inputHarga} onChange={handleChange}/>&nbsp;
              <label>
                Masukkan Berat Buah:
              </label>          
              <input type="text" name="berat" value={inputBerat} onChange={handleChange}/><br></br><br></br>
              <button>submit</button><br></br>
            </form>
          </div>
        </>
      )
}

export default Lists