import React, { useState, createContext } from "react";

export const ColorThemeContext = createContext();

export const ColorThemeProvider = props => {
    const [color, setColor] = useState(["#e169b4", "#ca5ea2", "#f6d2e8", "#69e1ae", "#776cf4","#6cf1f4","#a1f46c"])

    return(
        <ColorThemeContext.Provider value={[color, setColor]}>
            {props.children}
        </ColorThemeContext.Provider>
    )
}

export default ColorThemeContext