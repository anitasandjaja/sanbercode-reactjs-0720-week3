import React, { useState, useEffect, createContext } from "react";
import axios from 'axios'

export const HargaBuahContext = createContext();

export const HargaBuahProvider = props => {
    const [dataHargaBuah, setDataHargaBuah] =  useState(null)

    useEffect( () => {
        if (dataHargaBuah === null){
          axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
          .then(res => {
            setDataHargaBuah(res.data.map(el=>{ return {id: el.id, nama: el.name, harga: el.price, berat: el.weight}} ))
          })
        }
      }, [dataHargaBuah])

  return (
    <HargaBuahContext.Provider value={[dataHargaBuah, setDataHargaBuah]}>
      {props.children}
    </HargaBuahContext.Provider>
  );
};

export default HargaBuahContext