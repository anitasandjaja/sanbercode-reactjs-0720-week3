import React, { useState, Fragment, useEffect } from 'react';
import './table.css';
import Axios from 'axios';

const Table = (props) => {
    const [dataInput, setDataInput] = useState({ id: "", name: "", price: "", weight: "" });
    const [formStatus, setFormStatus] = useState("add");
    const [data, setData] = useState(null);

    useEffect(() => {
        if (data === null) {
            Axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
                .then(result => {
                    console.log(result);
                    setData(result.data);
                });
        }
    }, [data]);

    const editButtonHandler = (id) => {
        setFormStatus("edit");
        setDataInput(data.filter(data => data.id === id)[0]);
    }

    const deleteButtonHandler = (id) => {
        setData(data.filter(data => data.id !== id));

        Axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id} `)
            .then(res => {
                console.log(res)
            });
    }

    const handleSubmit = () => {

        if (formStatus === "edit") {
            let newData = data.map(data => {
                data = data.id === dataInput.id ? dataInput : data;
                return data;
            });
            Axios.put(`http://backendexample.sanbercloud.com/api/fruits/${dataInput.id}`, dataInput)
                .then(res => {

                    setData(newData);
                })
            setFormStatus("add");
            setDataInput({ id: "", name: "", price: "", weight: "" });
        } else if (formStatus === "add") {
            const newData = {
                ...dataInput
            }
            Axios.post(`http://backendexample.sanbercloud.com/api/fruits`, newData)
                .then(res => {
                    setData([...data, { ...newData, id: res.data.id }]);
                })
            setDataInput({ id: "", name: "", price: "", weight: "" });
        }
    }

    const handleNameChange = (e) => {
        setDataInput({
            ...dataInput,
            name: e.target.value
        });
    }

    const handlePriceChange = (e) => {
        setDataInput({
            ...dataInput,
            price: e.target.value
        });
    }

    const handleWeightChange = (e) => {
        setDataInput({
            ...dataInput,
            weight: e.target.value
        });
    }

    const cancelButtonHandler = () => {
        setFormStatus("add");
        setDataInput({ id: "", name: "", price: "", weight: "" });
    }

    return (
        <Fragment>
            <div className="table-container">
                <h1>{props.title}</h1>
                <table>
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Weight</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data !== null && data.map((databuah, key) => {
                            return (<tr>
                                <td>{key + 1}</td>
                                <td>{databuah.name}</td>
                                <td>{databuah.price}</td>
                                <td>{databuah.weight / 1000} kg</td>
                                <td>
                                    <button className="edit" onClick={() => editButtonHandler(databuah.id)}>Edit</button>
                                    <button className="hapus" onClick={() => deleteButtonHandler(databuah.id)}>Hapus</button>
                                </td>
                            </tr>)
                        })}
                    </tbody>
                </table>


                {/* ============form============ */}
                <form action="#">
                    <h2>Form input</h2>
                    <div className="formel">
                        <label htmlFor="name">name</label>
                        <input type="text" id="name" value={dataInput.name} onChange={handleNameChange} />
                    </div>
                    <div className="formel">
                        <label htmlFor="price">price</label>
                        <input type="number" id="price" value={dataInput.price} onChange={handlePriceChange} />
                    </div>
                    <div className="formel">
                        <label htmlFor="weight">weight</label>
                        <input type="number" id="weight" value={dataInput.weight} onChange={handleWeightChange} />
                    </div>
                    <div className="btns">
                        <button className="save" onClick={handleSubmit}>Simpan</button>
                        <button type="button" className="cancel" onClick={() => cancelButtonHandler("as")}>Batal</button>
                    </div>
                </form>
            </div>
        </Fragment>
    );
}

export default table;