import React from 'react';
import './TabelBuah.css';
const { render } = require("@testing-library/react")

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]
  class Nama extends React.Component {
    render() {
      return <p>Hello, {this.props.name}</p>;
    }
  }
  class Harga extends React.Component {
    render() {
      return <p>Hello, {this.props.harga}</p>;
    }
  }
  
  class Berat extends React.Component {
    render() {
      return <p>your age is {this.props.berat}</p>;
    }
  }
class TabelBuah extends React.Component { 
    render() {
        return(
            <>
            <h1>Harga Tabel Buah</h1>
            <table>
                <tr>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                </tr>
                {dataHargaBuah.map((post) =>{
                    return(
                    <tr>
                        <td>
                            <Nama name={post.nama}/>
                        </td>
                        <td>
                            <Harga harga={post.harga}/>
                        </td>
                        <td>
                            <Berat berat={post.berat}/>
                        </td>
                    </tr>
                    )
                })}
            </table>
            </>
        );
      }
}

export default TabelBuah;
   